FogBugz plugin for Greenshot
============================

Overview
--------
This is a plugin for the Greenshot screenshot tool for Windows. It allows you to easily send a screenshot to [FogBugz](https://www.fogcreek.com/FogBugz/) on demand.

Features
--------
* Mimics native FogBugz Screenshot application behaviour (more on that below)
* No configuration
* Fast - hotkey access, remembers stuff
* Secure - no credential handling

Demo
----
![Demo reel](https://bitbucket.org/dodgyrabbit/greenshotfogbugzplugin/raw/master/demo.gif "Demonstration animated gif")

No FogBugz API
--------------
Others have implemented FogBugz plugins for Greenshot, which is great. However, they all use the XML API for FogBugz. Sounds good right? Except - the API does not allow you to set up a case without actually **saving** it. In other words - there is no turning back once you click Send - it creates the case immediately in FogBugz and the user can't change their mind about it afterwards. Worse than that - it creates the case with whatever default project, priority and so forth was configured.

The native FogBugz Screenshot tool uses a different approach. It sends the screenshot and gives the user the opportunity to select priorty, project, who it's assigned to before actually creating the case. You can also cancel at that point without creating a new case, or without updating an existing case.

Secret Sauce
------------
Turns out there is no ways in the FogBugz API to achieve the native behaviour. I noticed however that whenever you use the native screenshot tool it creates a local HTML file in your temp directory, which is submitted to your on demant website at FogBugz. I managed to grab a copy of this file and see what they were up to. It was pretty straightforward really:

* It sets up a form with some variables (are you creating a new case or editing existing)
* It takes the image you're sending and base64 encodes it
* If the base64 encoding is longer than 100,000 characters, it splits up the image into sections
* It has JavaScript that automatically submits the form on load

Implementation
--------------
This plugin has an embedded resource that is basically the HTML template that contains almost everyting to submit a case. There are a couple of placeholders {...} that are replaced
at runtime when we've got the picture that we need to send. It breaks up the base64 encoding at 100,000 character intervals, just like the native implementation. This actually makes the plugin really simple as there's no need to use the FogBugz API at all.

Usage
-----
Capture your screen as per normal in Greenshot. Annotate and do whatever you need to do. You activate the plugin through the following ways:

* Click the toolbar Fogbugz icon
* File->Send to FogBugz
* `Ctrl + F` from the Editor Window

Once the Send to FogBugz dialog comes up, all you need to do is validate your on demand URL and hit Enter. You can also optionally enter an existing case number if you want to update an existing case.

Note that, like the native FogBugz screenshot tool - it requires you to be logged in to FogBugz. If you have not logged in, your browser will ask you to login first. Handling of credentials is not done by this plugin.

Hit ESC to cancel if you do not wish to send the case and close the window.

Installation
------------
Copy the GreenshotFogBugzPlugin.gsp file into the plugins folder for Greenshot. This is typically:
`C:\Program Files\Greenshot\Plugins\GreenshotFogBugzPlugin.gsp`

Note that the .gsp file is really just a .Net assembly (dll) that has been renamed. 
Presumably it means `g`reen`s`hot `p`lugin.