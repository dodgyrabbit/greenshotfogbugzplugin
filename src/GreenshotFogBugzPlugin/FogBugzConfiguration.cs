﻿using Greenshot.IniFile;

namespace GreenshotFogBugzPlugin
{
    [IniSection("FogBugz", Description = "Greenshot FogBugz Plugin configuration")]
    public class FogBugzConfiguration : IniSection
    {
        [IniProperty("FogBugzOnDemandName", Description = "Fogbugz On Demand name.", DefaultValue = "example")]
        public string FogBugzOnDemandName;

        [IniProperty("FogBugsLastCaseNumber", Description = "The previous case number that was entered.", DefaultValue = "0")]
        public int FogBugsLastCaseNumber;
    }
}
