﻿Installing the plugin
---------------------

The Greenshot FogBugs plugin installation is easy!
All you have to do is copy the GreenshotFogBugzPlugin.gsp file to the plugins folder under your Greenshot installation. You will need to restart Greenshot before it will pick up the new plugin.

If you have a standard installation (not portable version or multiple disk drives), this is going to be:

C:\Program Files\Greenshot\Plugins 
or
C:\Program Files (x86)\Greenshot\Plugins 

Note that some plugins have a seperate folder for the .gsp file, but this is not necessary. This plugin does not require any other files so it can live happily on it's own directly under \Plugins